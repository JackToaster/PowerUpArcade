shader_type canvas_item;

uniform vec2 redBleed = vec2(0.000,0);
uniform vec2 greenBleed = vec2(0.000, 0);
uniform vec2 blueBleed = vec2(0,0);
uniform vec3 bleedAmount = vec3(0,0,0);


uniform float scanLineCount = 256;
uniform float scanLineWidth = 0.5;
uniform float scanLineWeight = 0.02;
uniform float scanLineSpeed = 16.3123;

uniform float barrelPower = 1.02;

vec3 bleed(sampler2D tex, vec2 uvInput){
	lowp vec3 outCol = texture(tex, uvInput).rgb;
	lowp vec3 bleedOut = vec3(1.0);
	bleedOut.r = texture(tex, uvInput + redBleed).r;
	outCol.r = mix(outCol.r, bleedOut.r, bleedAmount.r);
	bleedOut.g = texture(tex, uvInput + greenBleed).g;
	outCol.g = mix(outCol.g, bleedOut.g, bleedAmount.g);
	bleedOut.b = texture(tex, uvInput + blueBleed).b;
	outCol.b = mix(outCol.b, bleedOut.b, bleedAmount.b);
	
	return outCol;
}

vec2 distortUv(vec2 uvInput){

	vec2 centered = 2.0 * uvInput - vec2(1.0);
	float angle = atan(centered.y/centered.x);
	if(centered.x < 0.0){
		angle += 3.1415926535;
	}
	float dist = pow(length(centered), barrelPower);
	centered = vec2(dist * cos(angle), dist * sin(angle));
	return 0.5 * (centered + vec2(1.0));
}

float scanLines(vec2 uvInput, float time){
	float outWeight = scanLineWeight;

	return outWeight * float(fract(uvInput.y * scanLineCount + time * scanLineSpeed) > scanLineWidth);
}

vec3 quantize(vec3 inCol){
	return vec3(0.1) + smoothstep(0.1,0.7,inCol) * 0.65 + smoothstep(0.7,0.9,inCol) * 0.25;
//	+ smoothstep(0.4,0.6,inCol) * 0.2 + smoothstep(0.6,0.8,inCol) * 0.2
//	+ smoothstep(0.8,1,inCol) * 0.2;
}

void fragment() {
	vec2 distorted = distortUv(UV);
	
	lowp vec3 outCol = bleed(TEXTURE, distorted);
	
	outCol = mix(outCol, vec3(0), scanLines(distorted, TIME));
	
	COLOR.rgb = quantize(outCol);
}