# Power Up Arcade game
This is an arcade game designed to run on The Flying Toasters' arcade cabinet.

## Project goals
1. Playable power up game
2. High scores stored
3. 1 and 2 player mode
4. AI opponent
